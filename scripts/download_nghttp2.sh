#!/bin/bash

# If any commands fail, fail the script immediately.
set -ex

# Clone the repository to the specified directory.
# git clone --branch v1.41.0 https://github.com/nghttp2/nghttp2 $1
git clone --branch rehuffchar https://gitlab.com/xichixingman/nghttp2-custom $1
# cd $1 && git reset --hard a6ce8ccad28b593860b865ae178a74a41f0e4b33